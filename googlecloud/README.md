Ansible Role: googlecloud
=========

A role for installing the Goolge Cloud CLI and auth plugin

Requirements
------------

This role has no specific requirements.

Role Variables and Tags
-----------------------

*Variables*
- uninstall: set to true to remove tagged or all components

*Tags*
- googlecloud: For all tasks in the googlecloud role
- googlecloud_apt_config: For the apt configuration tasks
- googlecloud_install: For the CLI installation tasks

Dependencies
------------

This role has no specific dependencies.

Example Playbook Snippet
------------------------
```
  tasks:
    - name: Install the Google Cloud CLI and auth plugin
      import_role:
        name: googlecloud
```
