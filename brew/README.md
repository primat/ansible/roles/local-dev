# Ansible Role: brew

Installs Homebrew packages for Mac.

## Requirements

Requires Homebrew to be installed already.

## Role Variables and Tags

_Variables_

- brew_casks: The list of casks to install (or uninstall)
- brew_formulae: the list of formulae to install (or uninstall)
- uninstall: set to true to remove packages

_Tags_

- brew: For running all tasks in the role
- brew_casks: For running all tasks related to casks
- brew_formulae: For running all tasks related to formulae

## Dependencies

This role has no dependencies.

## Example Playbook Snippet

```
  tasks:
    - name: Install Homebrew packages and casks
      import_role:
        name: brew
      var:
        brew_casks:
          - dotnet-sdk
          - mongodb-compass
          - visual-studio-code
        brew_formulae:
          - curl
          - make
          - zip
```
