Ansible Role: tilt
=========

Role for installing and configuring Tilt

Requirements
------------

No specific requirements. The role will not add the folder wher the tilt binary is to the $PATH so this will have to be done elsewhere

Role Variables and Tags
-----------------------

*Variables*
- tilt_install_dir: The directory to install the tilt binary
- tilt_version: The tilt version to install
- tilt_platform: the platform of the binary
- tilt_architecture: the architecture of the binary
- tilt_archive: the name of the archive to download and extract the binary from
- tilt_archive_url: The URL where to download the archive
- tilt_namespace: If non-empty, will export TILTNAMESPACE in .bashrc or other profile

*Tags*
- tilt: run all tasks in the tilt role
- tilt_install: for installation specific tasks
- tilt_config: for configuration specific tasks


Dependencies
------------

This role has no specific dependencies.

Example Playbook Snippet
------------------------
```
  tasks:
    - name: Install Tilt
      import_role:
        name: tilt
```
