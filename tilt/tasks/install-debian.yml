---

- name: Register Tilt installed version string
  command: "{{ tilt_install_dir }}/tilt version"
  register: tilt_installed_version_result
  changed_when: false
  check_mode: false
  ignore_errors: true
  tags: 
    - tilt
    - tilt_install

- name: Extract Tilt version from string
  set_fact:
    tilt_installed_version: "{{ tilt_installed_version_result.stdout.split(',')[0].split('v')[1] }}"
  when: tilt_installed_version_result and tilt_installed_version_result.rc == 0
  tags:
    - tilt
    - tilt_install

- name: Compare installed Tilt version with desired version
  assert:
    that: tilt_version == tilt_installed_version
    fail_msg: "Installed Tilt version is not the desired version. Current version: {{ tilt_installed_version }}. Desired version: {{ tilt_version }}"
    success_msg: "Installed Tilt version matches the desired version {{ tilt_installed_version }}"
  failed_when: false
  when: tilt_installed_version is defined
  tags: 
    - tilt
    - tilt_install

- name: Create Tilt bin directory if it doesn't exist
  file:
    path: "{{ tilt_install_dir }}"
    state: directory
  when: not (uninstall | default(false) | bool)
  tags:
    - tilt
    - tilt_install

- name: Download Tilt
  get_url:
    url: "{{ tilt_archive_url }}"
    dest: "/tmp/{{ tilt_archive}}"
  when: not (uninstall | default(false) | bool) and (tilt_installed_version_result.rc != 0 or tilt_version != tilt_installed_version)
  tags: 
    - tilt
    - tilt_install

- name: Remove existing Tilt installation
  file:
    path: "{{ tilt_install_dir }}/tilt"
    state: absent
  when: (uninstall | default(false) | bool) or (tilt_installed_version is defined and tilt_version != tilt_installed_version)
  tags:
    - tilt
    - tilt_install

- name: Extract Tilt from archive
  unarchive:
    src: "/tmp/{{ tilt_archive }}"
    dest: "{{ tilt_install_dir }}/"
    remote_src: true
    list_files: true
    mode: 0755
    include:
      - tilt
  when: not (uninstall | default(false) | bool) and (tilt_installed_version_result.rc != 0 or tilt_version != tilt_installed_version)
  tags:
    - tilt
    - tilt_install
