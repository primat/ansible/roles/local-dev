Ansible Role: common
=========

Configure common settings for local development machines.

Requirements
------------

There are no requirements for this role.

Role Variables and Tags
-----------------------

*Variables*
- common_user_bin_dir: The location where user scripts and binaries are installed
- uninstall: Set to true to uninstall or deconfigure settings and scripts

*Tags*
- common: For running all tasks in the role
- common_user_bin_dir: The directory where user binaries will get installed
- common_scripts: For tasks that install useful script for development environemnts

Dependencies
------------

There are no dependencies for this role. 

Example Playbook Snippet
------------------------
```
  tasks:
    - name: Configure common settings
      import_role:
        name: common
      vars:
        common_user_bin_dir: "{{ ansible_env.HOME }}/.local/bin"
```
