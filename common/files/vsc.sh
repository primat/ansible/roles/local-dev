#!/bin/bash

# This script provides a shortcut to be able to open a project in VS Code
# without having to drill down a tonne of directories

# List all directories under $HOME/code that contain a .git folder
options=$(find "$HOME/code" -type d -name ".git" | rev | cut -c 6- | rev | sed "s|$HOME/code/||" | sort)

PS3="Choose a Git project to open in VS Code: "

# Display the options to the user
select folder in $options; do
    if [ -n "$folder" ]; then
        selected_folder="$HOME/code/$folder"
        echo "Opening $selected_folder"
        # Added this because it's slow to start VSCode form a Linux folder, I don't know why...
        cd /mnt/c/Users
        code "$selected_folder"
        break
    else
        echo "Invalid option. Please select again."
    fi
done
