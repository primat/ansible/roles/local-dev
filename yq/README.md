Ansible Role: yq
=========

Role for installing yq

Requirements
------------

Some tasks in the role require root privilieges since it installs yq to /usr/local/bin.

Role Variables and Tags
-----------------------

*Variables*
- yq_version: Version of the release to install
- yq_architecture: Architecture of the static binary
- yq_bin_url: URL to the archive of the release to install
- unsintall: set to true to remove yq

*Tags*
- yq: run all tasks in the yq role

Dependencies
------------

There are no dependencies for this role.

Example Playbook Snippet
------------------------
```
  tasks:
    - name: Install yq
      import_role:
        name: yq
```
