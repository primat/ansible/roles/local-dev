Ansible Role: protobuf
=========

Role for installing protocol buffers

Requirements
------------

This role requires the zip utility to be installed since that is the format of the archive the maintainers use.

Role Variables and Tags
-----------------------

*Variables*
- protobuf_bin_dir: the directory where the protobuf compiler gets installed
- protoc_version: The version of the binary to install
- protoc_platform: The binary platform
- protoc_architecture: The binary architecture
- protoc_archive: The name of the archive
- protoc_archive_url: The URl where to download the archive
- uninstall: set to true to remove configured npm packages

*Tags*
- protobuf: For all tasks in the npm role
- protoc_install: For the compiler installation tasks only

Dependencies
------------

There are no dependencies for this role.

Example Playbook
----------------
```
  tasks:
    - name: Install protobuf compiler and components
      import_role:
        name: protobuf
      vars:
        protobuf_bin_dir: "{{ ansible_env.HOME }}/.local/bin"
        protoc_version: "26.1"
        protoc_platform: linux
        protoc_architecture: x86_64
        protoc_archive: protoc-{{ protoc_version }}-{{ protoc_platform }}-{{ protoc_architecture }}.zip
        protoc_archive_url: https://github.com/protocolbuffers/protobuf/releases/download/v{{ protoc_version }}/{{ protoc_archive }}
```
