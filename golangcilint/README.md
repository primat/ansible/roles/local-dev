# Ansible Role: golangcilint

Role for installing and configuring golangci-lint, a Go linter. This role is probably only useful for Linux machines since MAc and Windows can install it using Homebrew and Chocolatey, respectively.

## Requirements

This role has no specific requiresments and does not use the `go install` command to install golangci-lint, as recommended by the golangci-lint team.

## Role Variables and Tags

_Variables_

- golangcilint_bin_dir: The location to install the binary
- golangcilint_version: The version to install
- golangcilint_platform: The platform of the binary
- golangcilint_architecture: The architecture of the binary
- golangcilint_archive: The archive name containing the binary
- golangcilint_archive_url: The URL of the archive
- uninstall: set to true to remove tagged or all components

_Tags_

- golangcilint: run all tasks in the golangcilint role

## Dependencies

There are no dependencies for this role.

## Example Playbook Snippet

```
  tasks:
    - name: Install and configure golangci-lint
      import_role:
        name: golangcilint
      vars:
        golangcilint_bin_dir: "{{ ansible_env.HOME}}/.local/bin"
        golangcilint_version: 1.57.2
        golangcilint_platform: linux
        golangcilint_architecture: amd64
        golangcilint_archive: "golangci-lint-{{ golangcilint_version }}-{{ golangcilint_platform }}-{{ golangcilint_architecture }}.tar.gz"
        golangcilint_archive_url: "https://github.com/golangci/golangci-lint/releases/download/v{{ golangcilint_version }}/{{ golangcilint_archive}}"
```
