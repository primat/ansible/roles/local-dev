# Ansible Role: npm

Role for installing global npm packages. This role does not install npm itself. IT already comes bundled with Node.

## Requirements

NPM must be installed for this role to function

## Role Variables and Tags

_Variables_
The roles takes a list of packages to install, along with an optional version

- npm_packages:
  - name: The name of the npm package, e.g. typescript
  - version: An optional version. It not defined, will pull the latest. The value can also be 'latest' or a specific semver to install.
- npmrc_profile: a list of entries (e.g. configs to private registries) to add to the .npmrc file

The role also takes a list of npmrc profile to configure in .npmrc
npmrc_profiles:

- scope: The scope of the package registry, e.g. @something
  registry: The registry URL, excluding the protocol (hardcoded to https://)
  token: (Optional) The auth token to use for authentication

Packages can also be uninstalled when using the uninstall=true variable/value.

- uninstall: set to true to remove configured npm packages

_Tags_

- npm: For all tasks in the npm role
- npm_packages: For the npm packages tasks
- npm_npmrc: For the npmrc configuration tasks

## Dependencies

There are no dependencies for this role.

## Example Playbook

```
  tasks:
    - name: Install global npm packages
      import_role:
        name: npm
```
