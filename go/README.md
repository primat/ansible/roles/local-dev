# Ansible Role: go

Role for installing and configuraing Go

## Requirements

If not using this role to install go, then some tasks require that Go already be installed.

## Role Variables and Tags

_Variables_

- uninstall: set to true to remove tagged or all components, excluding GOPATH files and folders

_Tags_

- go: run all tasks in the go role
- go_install: for the installation task(s)
- go_config: for all configuration tasks
- go_gopath: for GOPATH configuration
- go_goprivate: for GOPRIVATE configuration

## Dependencies

There are no dependencies for this role.

## Example Playbook

```
  tasks:
    - name: Install and configure Go
      import_role:
        name: go
```
