Ansible Role: gobins
=========

Role for installing GO binaries (using `go install`)

Requirements
------------

Requires that Go is installed and a GOPATH where to install the binaries. If you want to specify `latest` as the version and provide a github API URL to the the list of latest versions, the cURL is also required.

Role Variables and Tags
-----------------------

*Variables*
- gobins: a list of (objects) packages to install. The object have the following spec:
  - bin_name: The name of the binary that gets installed, e.g. `protoc-gen-go`
  - version: The version of the package, or, `latest`. Using `latest` requires a `tags_api_url` otherwise it won't work
  - tags_api_url: (optional) The github tags API URL, e.g. `https://api.github.com/repos/protocolbuffers/protobuf-go/tags`
  - pkg_url: The URL of the package (as used in the `go install` command, e.g. `google.golang.org/protobuf/cmd/protoc-gen-go`) 
- gobins_filter: provide this value on the command line as an extra-vars, comma separated list of bin_names. This will filter the task to run only the listed bin_names.
- uninstall: set to true to remove tagged or identified binaries. Combine this with `gobins_filter` to uninstall specific binaries.

*Tags*
- gobins: run all tasks in the golangcgobinsilint role

Dependencies
------------

This role does not depend on any other Ansible resources.

Example Playbook Snippet
------------------------
```
  tasks:
    - name: Install and configure golangci-lint
      import_role:
        name: golangci-lint
```
