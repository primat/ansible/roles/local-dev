Ansible Role: exrc
=========

This role (currently) creates a .exrc file in the user's home directory and adds some directives to make the terminal usable. Only nedeed for Debian WSL instances.

Requirements
------------

There are no specific requirements.

Role Variables and Tags
-----------------------

*Variables*
- uninstall: set to true to remove the added lines and delete the ~/.exrc file if it's empty.

*Tags*
- exrc: run all tasks

Dependencies
------------

There are no dependencies.

Example Playbook Snippet
------------------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: username.rolename, x: 42 }

License
-------

BSD

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
