# Ansible Role: git

This role is used to install and configure Git. It also clones configured repositories.

## Requirements

This role requires that Git is installed, if it doesn't install Git itself.

This role configures Git for use with multiple accounts. It requires that all projects, for a distinct Git account, be placed under the same folder since the role will create a .gitconfig file that applies to all projects in subdirectories. The global .gitconfig (i.e. the one at ~/.gitconfig) will conditionally include the account-specific .gitconfig based on the path where they are stored. This enables the mangement of multiple repositories, for example where there may be a need to use personal and work repositories on the same domain.

This role will perform Git operations on configured repositories. If "git" is used as the communication protocol, then SSH profiles are required. These SSH profiles need to be created beforehand. This role does not manage the SSH configuration.

## Role Variables and Tags

_Variables_
The role expects a list of git projects with some specific details. Within the list of projects there should be a list of repos. The projects are grouped so that distinct .gitconfigs can apply to them.

```
git_projects:
  - account_id: A unique identifier for this group of projects
    account_dir: The root directory where the projects are stored and the account specific .gitconfig is created
    user: The Git user
    email: The Git email address
    repos: A list of repos belonging to this group
      - repo: The URL of the Git repo
        dest: The directory where the repo will be cloned to, relative to the above account_dir
        version: the default branch to checkout
        pull: Whether or not to pull changes once the repo is cloned.
      - ... (more repos)
  - ... (more accounts)

Additionally, a list of extra git configurations can be specified.
git_configs:
  - name: The name of the Git config
    value: The value of the Git config
    scope: (Optional) The scope where the config will be applied, defaults to `global`. If set to `local`, the repo value must be provided
    repo: The path to the Git repo where the config gets applied
```

_Tags_

- git: run all tasks in the git role
- go_config: for all configuration tasks
- go_clone: For the task(s) that clone the configured repos

## Dependencies

This role has no dependencies.

## Example Playbook Snippet

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

```
  tasks:
    - name: Set up Git and clone projects
      import_role:
        name: git
```
