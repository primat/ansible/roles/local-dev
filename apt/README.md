# Ansible Role: apt

Installs simple packages that don't require any special configuration and can be updated always to the latest version. e.g. cURL, zip, Make, etc

## Requirements

This require apt installed on the system.

## Role Variables and Tags

_Variables_

- apt_packages: the list of packages to install (or uninstall)
- uninstall: set to true to remove packages

_Tags_

- apt: for running all tasks in the role

## Dependencies

This role has no dependencies.

## Example Playbook Snippet

```
  tasks:
    - name: Install apt packages
      import_role:
        name: apt
      var:
        apt_packages:
          - curl
          - make
          - zip
```
