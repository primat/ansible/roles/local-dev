Ansible Role: ssh
=========

A role for automating the maintenance of profiles (or hosts) in the .ssh/config

Requirements
------------

Keys should already be added to the ~/.ssh folder if they are to be configured. This role does not create any keys nor does it consume any secrets for producing them. 

Role Variables and Tags
-----------------------

*Variables*
The role consumes a list of "ssh_profiles" that are use to produce profiles, or hosts, in the ~/.ssh/config file/
```
ssh_profiles:
  - alias: An alias for the server to connect to. This is useful when ther are multiple profiles for the same HostName
    user: The value for the user directive
    hostname: The value for the HostName directive
    ssh_key: The name of the private SSH key. It must reside in the ~/.ssh folder
    directives: A list of additional directives
```

*Tags*
- ssh: run all tasks in the ssh role

Dependencies
------------

This role has no dependencies.

Example Playbook Snippet
------------------------
```
  tasks:
    - name: Configure SSH
      import_role:
        name: ssh
```
