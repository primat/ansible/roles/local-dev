# Ansible Role: npm

Role for installing nvm, the Node version manager

## Requirements

When used on a Mac, this role requires Homebrew.

## Role Variables and Tags

_Variables_

- uninstall: set to true to remove nvm and nvm configurations

_Tags_

- nvm: To run all tasks in the nvm role
- nvm_install: Runs only the installation tasks
- nvm_config: Runs only the configuration tasks

## Dependencies

There are no dependencies for this role.

## Example Playbook

```
  tasks:
    - name: Install nvm
      import_role:
        name: nvm
```
